@extends('layouts.app')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row page-title align-items-center">
            <div class="col-sm-4 col-xl-6">
                <h4 class="mb-1 mt-0">Dashboard</h4>
            </div>
            <div class="col-sm-8 col-xl-6">
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <h5 class="header-title mb-1 mt-0">Jadwal Pegawai</h5>
                            @if ($jadwal)
                            <div id="demo" class="carousel slide" data-bs-ride="carousel">
                                <ol class="carousel-indicators">
                                    @foreach ($jadwal as $keys => $items)
                                    <li data-target="#demo" data-slide-to="{{$keys}}" class="{{$keys == 0 ? 'active' : ''}}"></li>
                                    @endforeach
                                </ol>
                                <!-- The slideshow/carousel -->
                                <div class="carousel-inner">
                                    @foreach ($jadwal as $key => $item)
                                    <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                                        <img src="{{asset('foto/jadwal/'. $item->lampiran)}}" alt="" style="100%" class="d-block w-100">
                                    </div>
                                    @endforeach
                                  
                                </div>
                                {{-- <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                  </button>
                                  <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                  </button> --}}
                              </div>
                            <!-- Carousel -->
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</div> <!-- content -->
@endsection
@section('js')
<script>
    $('.carousel').carousel()
</script>
@endsection