@extends('layouts.app')

@section('css')
    <!-- plugin css -->
    <link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" href="{{ asset('assets/sweetalert2/sweetalert2.css') }}"/>
@endsection

@section('content')
<div class="content">
                    
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Jadwal Pegawai</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">Data Jadwal Pegawai</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <button onclick="addForm()" type="button" class="btn btn-primary width-md"><i data-feather="plus-circle"></i> Tambah Data</button><br><br>

                        <table id="data-jadwal" class="table dt-responsive nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th style="width: 10%">No</th>
                                    <th style="width: 20%">Judul</th>
                                    <th style="width: 30%">Lampiran</th>
                                    <th style="width: 10%">Aksi</th>
                                </tr>
                            </thead>
                        
                        
                            <tbody>
                                @foreach ($data as $datas)
                                <tr>
                                    <td>{{++$no}}</td>
                                    <td>{{$datas->judul}}</td>
                                    <td>
                                        <img src="{{asset('foto/jadwal/'.$datas->lampiran)}}" alt="" width="100%">
                                        {{-- {{$datas->lampiran}} --}}
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button onclick="editForm('{{$datas->id}}')" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Data"><i data-feather="edit"></i></button>
                                            <button onclick="deleteData('{{$datas->id}}')" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Data"><i data-feather="trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->

    </div> <!-- container-fluid -->

</div> <!-- content -->
@include('jadwal.form')
@endsection

@section('js')
    <!-- datatable js -->
    <script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    
    <script src="{{ asset('assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.select.min.js') }}"></script>

    <!-- Datatables init -->
    <script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>

    <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        var table = $('#data-jadwal').DataTable();

        function addForm() {
          save_method = "add";
          $('input[name=_method]').val('POST');
          $('#modal-form').modal('show');
          $('.modal-title').text('Tambah Data Jadwal Pegawai');
          $('#modal-form form')[0].reset();
        }

        function editForm(id) {
          save_method = 'edit';
          $('input[name=_method]').val('POST');
          $('#modal-form form')[0].reset();
          $.ajax({
            url: "{{ url('jadwal/edit') }}" + '/' + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
             console.log(data.data);
              $('#modal-form').modal('show');
              $('.modal-title').text('Edit Data Jadwal Pegawai');
    
              $('#id').val(data.data.id);
              $('#judul').val(data.data.judul);
            },
            error : function() {
                alert("Nothing Data");
            }
          });
        }
    
        function deleteData(id){
          var csrf_token = $('meta[name="csrf-token"]').attr('content');
          swal({
              title: 'Apa anda yakin?',
              text: "Anda tidak dapat membatalkan perintah ini!",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yakin!'
          }).then(function () {
              $.ajax({
                  url : "{{ url('jadwal/delete') }}" + '/' + id,
                  type : "GET",
                  data : {'_method' : 'GET', '_token' : csrf_token},
                  success : function(data) {
                    window.location.reload();
                      swal({
                          title: 'Success!',
                          text: data.message,
                          icon: 'success',
                          timer: '1500'
                      })
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '1500'
                      })
                  }
              });
          });
        }

    
        $(function(){
              $('#modal-form').on('submit', function (e) {
                  if (!e.isDefaultPrevented()){
                      var id = document.getElementById('id').value;
                      if (save_method == 'add') url = "{{ url('jadwal') }}";
                      else url = "{{ url('jadwal/update') . '/' }}" + id;
                      console.log(id);
                      $.ajax({
                          url : url,
                          type : "POST",
                          //data : $('#modal-form form').serialize(),
                          data : new FormData($('#modal-form form')[0]),
                          contentType : false,
                          processData : false,
                          success : function(data) {
                              $('#modal-form').modal('hide');
                              swal({
                                  title: 'Success!',
                                  text: data.message,
                                  icon: 'success',
                                  timer: '1500'
                                })
                            window.location.reload();
                          },
                          error : function(){
                              swal({
                                  title: 'Oops...',
                                  text: data.message,
                                  icon: 'error',
                                  timer: '1500'
                              })
                          }
                      });
                      return false;
                  }
              });
          });
    </script>
@endsection