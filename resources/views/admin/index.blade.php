@extends('layouts.app')

@section('css')
    <!-- plugin css -->
    <link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" href="{{ asset('assets/sweetalert2/sweetalert2.css') }}"/>

    <style>
        #overlay{	
      position: fixed;
      top: 0;
      z-index: 9999;
      width: 100%;
      height:100%;
      display: none;
      background: rgba(0,0,0,0.6);
    }
    .cv-spinner {
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;  
    }
    .spinner {
      width: 40px;
      height: 40px;
      border: 4px #ddd solid;
      border-top: 4px #2e93e6 solid;
      border-radius: 50%;
      animation: sp-anime 0.8s infinite linear;
    }
    @keyframes sp-anime {
      100% { 
        transform: rotate(360deg); 
      }
    }
    .is-hide{
      display:none;
    }
    </style>
@endsection

@section('content')
<div id="overlay">
    <div class="cv-spinner">
      <span class="spinner"></span>
    </div>
  </div>
<div class="content">
                    
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Admin</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">Data Admin</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div>
                            <button onclick="addForm()" type="button" class="btn btn-primary width-md"><i data-feather="plus-circle"></i> Tambah Data</button><br><br>
                        </div>

                        <table id="data-pegawai" class="table dt-responsive nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <th style="width: 10%">No</th>
                                    <th style="width: 30%">Nama</th>
                                    <th style="width: 30%">Username</th>
                                    <th style="width: 10%">Aksi</th>
                                </tr>
                            </thead>
                        
                        
                            <tbody>
                                @foreach ($data as $datas)
                                <tr>
                                    <td>{{++$no}}</td>
                                    <td>{{$datas->name}}</td>
                                    <td>{{$datas->email}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button onclick="editForm('{{$datas->id}}')" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit Data"><i data-feather="edit"></i></button>
                                            <button onclick="deleteData('{{$datas->id}}')" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Data"><i data-feather="trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->

    </div> <!-- container-fluid -->

</div> <!-- content -->
@include('admin.form')
@endsection

@section('js')
    <!-- datatable js -->
    <script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    
    <script src="{{ asset('assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.select.min.js') }}"></script>

    <!-- Datatables init -->
    <script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>

    <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>

    <script type="text/javascript">
        var table = $('#data-pegawai').DataTable();

        function addForm() {
          save_method = "add";
          $('input[name=_method]').val('POST');
          $('#modal-form').modal('show');
          $('.modal-title').text('Tambah Data Admin');
          $('#modal-form form')[0].reset();
        }

        function importForm() {
          save_method = "add";
          $('input[name=_method]').val('POST');
          $('#import-form').modal('show');
          $('.modal-title').text('Import Data Admin');
          $('#import-form form')[0].reset();
        }

        function editForm(id) {
          save_method = 'edit';
          $('input[name=_method]').val('POST');
          $('#modal-form form')[0].reset();
          $.ajax({
            url: "{{ url('admin/edit') }}" + '/' + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
              $('#modal-form').modal('show');
              $('.modal-title').text('Edit Data Admin');
              $('#id').val(data.id);
              $('#username').val(data.username);
              $('#nama').val(data.name);
              document.getElementById('password').require = false;
            },
            error : function() {
                alert("Nothing Data");
            }
          });
        }
    
        function deleteData(id){
          var csrf_token = $('meta[name="csrf-token"]').attr('content');
          swal({
              title: 'Apa anda yakin?',
              text: "Anda tidak dapat membatalkan perintah ini!",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yakin!'
          }).then(function () {
              $.ajax({
                  url : "{{ url('admin/delete') }}" + '/' + id,
                  type : "GET",
                  data : {'_method' : 'GET', '_token' : csrf_token},
                  success : function(data) {
                    window.location.reload();
                      swal({
                          title: 'Success!',
                          text: data.message,
                          icon: 'success',
                          timer: '1500'
                      })
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '1500'
                      })
                  }
              });
          });
        }

    
        $(function(){
              $('#modal-form').on('submit', function (e) {
                  if (!e.isDefaultPrevented()){
                      let id = $('#id').val();
                      if (save_method == 'add') url = "{{ url('admin/save/') }}";
                      else url = "{{ url('admin/update') . '/' }}" + id;
    
                      $.ajax({
                          url : url,
                          type : "POST",
                          //data : $('#modal-form form').serialize(),
                          data : new FormData($('#modal-form form')[0]),
                          contentType : false,
                          processData : false,
                          success : function(data) {
                              $('#modal-form').modal('hide');
                              swal({
                                  title: 'Success!',
                                  text: data.message,
                                //   icon: 'success',
                                  timer: '1500'
                                })
                            window.location.reload();
                          },
                          error : function(){
                              swal({
                                  title: 'Oops...',
                                  text: data.message,
                                //   icon: 'error',
                                  timer: '1500'
                              })
                          }
                      });
                      return false;
                  }
              });
          });
        
    </script>
@endsection