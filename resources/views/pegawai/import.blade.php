<!-- sample modal content -->
<div id="import-form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="basic-form" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                {{-- {{ csrf_field() }} {{ method_field('POST') }} --}}
                @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Daftar Pegawai</label>
                        <input type="file" class="form-control" id="file" name="file" required>
                        <small>*Hanya Menerima File Dalam Bentuk .xls, .xlsx</small>
                    </div>
                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->