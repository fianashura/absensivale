@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/sweetalert2/sweetalert2.css') }}"/>
<style>
    #overlay{	
  position: fixed;
  top: 0;
  z-index: 100;
  width: 100%;
  height:100%;
  display: none;
  background: rgba(0,0,0,0.6);
}
.cv-spinner {
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;  
}
.spinner {
  width: 40px;
  height: 40px;
  border: 4px #ddd solid;
  border-top: 4px #2e93e6 solid;
  border-radius: 50%;
  animation: sp-anime 0.8s infinite linear;
}
@keyframes sp-anime {
  100% { 
    transform: rotate(360deg); 
  }
}
.is-hide{
  display:none;
}
</style>
@endsection
@section('content')
<div id="overlay">
    <div class="cv-spinner">
      <span class="spinner"></span>
    </div>
  </div>
<div class="content">
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Absen</li>
                    </ol>
                </nav>
                {{-- <h4 class="mb-1 mt-0">Basic Forms</h4> --}}
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-2 text-center">
                                <img src="{{ asset('assets/images/logo.png') }}" alt="">
                            </div>
                            <div class="col-lg-5 text-center">
                                <p class="sub-header">PROJECT MINE HAULING ROAD IGP BAHODOPI</p>
                                <h4 class="header-title">ABSENSI OPERATOR & DRIVER DT MHR</h4>
                            </div>
                            <div class="col-lg-5"></div>
                        </div>

                        <form id="absen-form" class="form-horizontal" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" id="device" name="device" value="{{$mobile}}">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                            <select class="form-control custom-select" id="kondisi" name="kondisi">
                                                <option value="Absen">Absen</option>
                                                <option value="Sakit">Sakit</option>
                                                <option value="Cuti">Cuti</option>
                                            </select>
                                            <label class="col-lg-10 col-form-label"></label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="simpleinput">ID Badge</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="id_badge" name="id_badge" style="background-color: rgb(238, 237, 237)" value="{{$pegawai->id_badge}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-email">Nama</label>
                                        <div class="col-lg-10">
                                            <input type="text" id="nama" name="nama" class="form-control" style="text-transform: uppercase;background-color: rgb(238, 237, 237)" value="{{$pegawai->nama}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-password">Posisi</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="posisi" name="posisi" style="text-transform: uppercase;background-color: rgb(238, 237, 237)" value="{{$pegawai->posisi}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Shift</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select" id="shift" name="shift">
                                                <option selected disabled>Pilih Shift</option>
                                                <option>Pagi</option>
                                                <option>Malam</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Absen</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select" id="absen" name="absen">
                                                <option selected disabled>Pilih Absen</option>
                                                <option value="Masuk">Masuk</option>
                                                <option value="Pulang">Pulang</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Waktu Mulai</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" placeholder="DD/MM/YYYY HH:ss" style="background-color: rgb(238, 237, 237)" value="" id="waktu_mulai" name="waktu_mulai" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Waktu Selesai</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" placeholder="DD/MM/YYYY HH:ss" style="background-color: rgb(238, 237, 237)" value="" id="waktu_selesai" name="waktu_selesai" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Lokasi Kerja</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" placeholder="Lokasi Anda Bekerja" id="lokasi_kerja" name="lokasi_kerja">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Titik Map</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="titik_map" name="titik_map" style="background-color: rgb(238, 237, 237)" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label"
                                            for="example-placeholder">No CT/Polisi *</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" style="text-transform: uppercase;" id="no_ct" name="no_ct">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">HM Awal *</label>
                                        <div class="col-lg-10">
                                            <input type="number" class="form-control" id="hm_awal" name="hm_awal" style="background-color: rgb(238, 237, 237)" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">HM Akhir *</label>
                                        <div class="col-lg-10">
                                            <input type="number" class="form-control" id="hm_akhir" name="hm_akhir" style="background-color: rgb(238, 237, 237)" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Catatan</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="catatan" name="catatan" required>
                                        </div>
                                    </div>
                                    @if ($mobile == 'Mobile')
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Selfie</label>
                                        <div class="col-lg-10">
                                            <input type="file" class="form-control" id="selfieFile" name="selfieFile" accept="image/*" capture="user">
                                        </div>
                                    </div>
                                    @else    
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Selfie</label>
                                        <div class="col-lg-5">
                                            <a id="start-camera-selfie" class="btn btn-info">Open Camera</a>
                                            <video id="video-selfie" width="100%" autoplay></video>
                                        </div>
                                        <div class="col-lg-5">
                                            <a id="click-photo-selfie" class="btn btn-info">Take Photo</a>
                                            <canvas id="canvas-selfie" width="100%" height="100%" style="width: 100%"></canvas>
                                            <input type="hidden" id="selfie" name="selfie" value="">
                                        </div>
                                    </div>
                                    @endif
                                    @if ($mobile == 'Mobile')
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Timesheet</label>
                                        <div class="col-lg-10">
                                            <input type="file" class="form-control" id="timesheetFile" name="timesheetFile" accept="image/*" capture="user">
                                        </div>
                                    </div>
                                    @else    
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="example-placeholder">Timesheet</label>
                                        <div class="col-lg-5">
                                            <a id="start-camera-timesheet" class="btn btn-info">Open Camera</a>
                                            <video id="video-timesheet" width="100%" autoplay></video>
                                        </div>
                                        <div class="col-lg-5">
                                            <a id="click-photo-timesheet" class="btn btn-info">Take Photo</a>
                                            <canvas id="canvas-timesheet" width="100%" height="100%" style="width: 100%"></canvas>
                                            <input type="hidden" id="timesheet" name="timesheet" value="">
                                        </div>
                                    </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary">Send</button>
                                </div>
                            </div>
                        </form>

                    </div> <!-- end card-body -->
                </div> <!-- end card-->
            </div><!-- end col -->
        </div>
        <!-- end row -->
        
    </div> <!-- container-fluid -->

</div> <!-- content -->
@endsection

@section('js')
<script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
    <script type="text/javascript">
    // console.log(kondisi);
    $("#kondisi").change(function(){
        let kondisi = $('#kondisi').val();
        if (kondisi !== 'Absen') {
            document.getElementById('shift').readOnly = true;
            $('#shift').attr('style', 'background-color: rgb(238, 237, 237)');
            document.getElementById('absen').readOnly = true;
            $('#absen').attr('style', 'background-color: rgb(238, 237, 237)');
            document.getElementById('waktu_mulai').readOnly = false;
            document.getElementById('waktu_mulai').type = 'date';
            document.getElementById('waktu_selesai').readOnly = false;
            document.getElementById('waktu_selesai').type = 'date';
            $('#waktu_mulai').attr('style', '');
            $('#waktu_selesai').attr('style', '');
            document.getElementById('lokasi_kerja').readOnly = true;
            $('#lokasi_kerja').attr('style', 'background-color: rgb(238, 237, 237)');
            document.getElementById('no_ct').readOnly = true;
            $('#no_ct').attr('style', 'background-color: rgb(238, 237, 237)');
        }
    });
    function imageSelfieCompress() {
        const fileSelfie = document.querySelector('#selfieFile');
        // fileSelfie.getContext('2d').drawImage(video_selfie, 0, 0, canvas_selfie.width, canvas_selfie.height);
        // console.log(fileSelfie.files[0]);
        let image_data_url_selfie = fileSelfie.files[0].readAsDataURL('image/jpeg');
        console.log(image_data_url_selfie);
    }

    const fileSelfie = document.querySelector('#selfieFile');

    // Listen for the change event so we can capture the file
    // fileSelfie.addEventListener('change', (e) => {
    //     // Get a reference to the file
    //     const file = e.target.files[0];

    //     // Encode the file using the FileReader API
    //     const reader = new FileReader();
    //     reader.readAsDataURL(file);
    //     reader.onloadend = () => {
    //         const img = new Image();
    //         img.src = event.target.result.toString();
      
    //         img.onload = () => {
    //           const elem = document.createElement('canvas');
    //           const scaleFactor = 50 / img.width;
    //           elem.width = 50;
    //           elem.height = img.height * scaleFactor;
      
    //           const ctx = elem.getContext('2d');
    //           ctx.drawImage(img, 0, 0, 50, img.height * scaleFactor);
      
    //           ctx.canvas.toBlob((blob) => {}, 'image/jpeg', 1);
    //         //   console.log(ctx);
    //         };
    //         // Logs data:<type>;base64,wL2dvYWwgbW9yZ...
    //     };
    //     console.log(reader);

    // });

    function readSelfie(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('#selfiePhoto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function readTimesheet(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('#timesheetPhoto').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    let device = $('#device').val();
    if(device !== 'Mobile') {
        let camera_button_selfie = document.querySelector("#start-camera-selfie");
        let video_selfie = document.querySelector("#video-selfie");
        let click_button_selfie = document.querySelector("#click-photo-selfie");
        let canvas_selfie = document.querySelector("#canvas-selfie");
        
        camera_button_selfie.addEventListener('click', async function() {
            let stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: false });
            video_selfie.srcObject = stream;
        });
    
        click_button_selfie.addEventListener('click', function() {
            canvas_selfie.getContext('2d').drawImage(video_selfie, 0, 0, canvas_selfie.width, canvas_selfie.height);
            let image_data_url_selfie = canvas_selfie.toDataURL('image/jpeg');
            $('#selfie').attr('value', image_data_url_selfie);
        });
        
        let camera_button_timesheet = document.querySelector("#start-camera-timesheet");
        let video_timesheet = document.querySelector("#video-timesheet");
        let click_button_timesheet = document.querySelector("#click-photo-timesheet");
        let canvas_timesheet = document.querySelector("#canvas-timesheet");
        
        camera_button_timesheet.addEventListener('click', async function() {
            let stream_timesheet = await navigator.mediaDevices.getUserMedia({ video: true , audio: false  });
            video_timesheet.srcObject = stream_timesheet;
        });
    
        click_button_timesheet.addEventListener('click', function() {
            canvas_timesheet.getContext('2d').drawImage(video_timesheet, 0, 0, canvas_timesheet.width, canvas_timesheet.height);
            let image_data_url_timesheet = canvas_timesheet.toDataURL('image/jpeg');
            $('#timesheet').attr('value', image_data_url_timesheet);
        });
    }

    $("#selfie").change(function(){
        readSelfie(this);
    });
    $("#timesheet").change(function(){
        readTimesheet(this);
    });

    $("#absen").change(function(){
        let absen = $('#absen').val();
        let date = new Date().toLocaleString('id-ID');
        if (absen === 'Masuk') {
            $.ajax({
                url: "{{ url('absensi/cekAbsen') }}" + '/' + absen,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    // console.log(data.data.waktu_masuk);
                    if(data.data && data.absensi === 'Sudah') {
                        alert('Anda Telah Melakukan Absen Masuk!');
                        $('#absen').val('');
                        $('#waktu_mulai').attr('value', '');
                        // $('#waktu_selesai').attr('value', date);
                        document.getElementById('hm_awal').readOnly = true;
                        document.getElementById('hm_awal').require = false;
                        document.getElementById('hm_akhir').readOnly = false;
                        document.getElementById('hm_akhir').require = true;
                        document.getElementById('hm_awal').value = '';
                        $('#hm_awal').attr('style', 'background-color: rgb(238, 237, 237)');
                        $('#hm_akhir').attr('style', '');
                    } else {
                        $('#waktu_mulai').attr('value', date);
                        $('#waktu_selesai').attr('value', '');
                        document.getElementById('hm_akhir').readOnly = true;
                        document.getElementById('hm_akhir').require = false;
                        document.getElementById('hm_awal').readOnly = false;
                        document.getElementById('hm_awal').require = true;
                        document.getElementById('hm_akhir').value = '';
                        $('#hm_awal').attr('style', '');
                        $('#hm_akhir').attr('style', 'background-color: rgb(238, 237, 237)');
                    }
                },
                error : function() {
                    alert('Something wrong!')
                }
            });
        } else {
            $.ajax({
                url: "{{ url('absensi/cekAbsen') }}" + '/' + absen,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    if(data.data.waktu_masuk == null || data.absensi === 'Belum') {
                        alert('Anda Belum Melakukan Absen Masuk!')
                        $('#absen').val('');
                        // $('#waktu_mulai').attr('value', date);
                        $('#waktu_selesai').attr('value', '');
                        document.getElementById('hm_akhir').readOnly = true;
                        document.getElementById('hm_akhir').require = false;
                        document.getElementById('hm_awal').readOnly = false;
                        document.getElementById('hm_awal').require = true;
                        document.getElementById('hm_akhir').value = '';
                        $('#hm_awal').attr('style', '');
                        $('#hm_akhir').attr('style', 'background-color: rgb(238, 237, 237)');
                    } else if(data.data.waktu_masuk != null && data.data.waktu_pulang != null) {
                        alert('Anda Telah Melakukan Absen Pulang!')
                    } else {
                        $('#waktu_mulai').attr('value', '');
                        $('#waktu_selesai').attr('value', date);
                        document.getElementById('hm_awal').readOnly = true;
                        document.getElementById('hm_awal').require = false;
                        document.getElementById('hm_akhir').readOnly = false;
                        document.getElementById('hm_akhir').require = true;
                        document.getElementById('hm_awal').value = '';
                        $('#hm_awal').attr('style', 'background-color: rgb(238, 237, 237)');
                        $('#hm_akhir').attr('style', '');
                        // $('#hm_awal').attr('value', '');
                    }
                    
                },
                error: function() {
                    alert('Something wrong!')
                }
            });
        }
    });

    const options = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 0,
    };

    
function success(pos) {
  const crd = pos.coords;

  $('#titik_map').attr('value', `${crd.latitude},${crd.longitude}`);
}

function error(err) {
  console.warn(`ERROR(${err.code}): ${err.message}`);
}

navigator.geolocation.getCurrentPosition(success, error, options);

$('#absen-form').submit(function(event) {
    // $(document).ajaxSend(function() {
        // $("#overlay").fadeIn(300);
//   });
    event.preventDefault();
    url = "{{ url('absensi/saveAbsen') }}";
    let formData = new FormData($('#absen-form')[0]);
    console.log(formData);
    // setTimeout(function() => {
        
    // }, timeout);
    $.ajax({
        url: url,
        type: 'POST',
        data: new FormData($('#absen-form')[0]),
        contentType : false,
        processData : false,
        timeout: 10000,
        success: function(data) {
            // setTimeout(function(){
                $("#overlay").fadeOut(300);
            // },500);
            swal({
                title: 'Success!',
                text: data.message,
                confirmButtonText: 'OK'
              }).then(function () {
                  window.location.href = "{{ route('absensi.logout') }}";
              });
        },
        error: function(xhr) {
            swal({
                title: 'Oops...',
                text: data.message,
              //   icon: 'error',
                timer: '1500'
            })
        }
    });
});
    </script>
@endsection