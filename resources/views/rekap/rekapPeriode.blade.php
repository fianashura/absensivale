@extends('layouts.app')

@section('css')
    <!-- plugin css -->
    <link href="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" href="{{ asset('assets/sweetalert2/sweetalert2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/leaflet/leaflet.css') }}"/>

    <style>
		.leaflet-container {
			height: 400px;
			width: 600px;
			max-width: 100%;
			max-height: 100%;
		}
	</style>

@endsection

@section('content')
<div class="content">
                    
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Rekap Absen </li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">Rekap Absensi {{date('d-m-Y', strtotime($from))}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <form action="{{ route('rekap.datePeriode') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-4">
                                            <input type="date" name="from" id="from" class="form-control" value="{{$from}}">
                                        </div>
                                        <div class="col-2 text-center">
                                            To
                                        </div>
                                        <div class="col-4">
                                            <input type="date" name="to" id="to" class="form-control" value="{{$to}}">
                                        </div>
                                        <div class="col-2">
                                            <button type="submit" class="btn btn-primary btn-md">Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-6">
                                <a href="{{ route('rekap.exportPeriode', [$from, $to]) }}" target="_blank" class="btn btn-primary btn-md width-md float-right"><i data-feather="share"></i> Export Excel</a> 
                            </div>
                        </div>
                        <br>
                        <table id="data-pegawai" class="table dt-responsive slide table-sm" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Profil</th>
                                    <th >Status Absen</th>
                                    <th >Absensi</th>
                                    <th >Pekerjaan</th>
                                    <th >Catatan</th>
                                    <th >Status</th>
                                    <th >Foto Masuk</th>
                                    <th >Foto Pulang</th>
                                </tr>
                            </thead>
                        
                        
                            <tbody>
                                @foreach ($data as $datas)
                                <tr>
                                    <td>{{++$no}}</td>
                                    <td>
                                        <b>ID Badge</b> : <br>{{$datas->id_badge}} <br>
                                        <b>Nama</b> : <br>{{$datas->nama}} <br>
                                        <b>Posisi</b> : <br>{{$datas->posisi}}
                                    </td>
                                    <td>
                                        <b>Jenis</b> : <br>{{$datas->kondisi}} <br>
                                        <b>Shift</b> : <br>{{$datas->shift}}
                                    </td>
                                    <td>
                                        <b>Masuk</b> : <br>
                                        @if ($datas->waktu_masuk == null)
                                            
                                        @else
                                        {{date('d-m-Y H:i', strtotime($datas->waktu_masuk))}}
                                        @endif
                                        <br>
                                        <b>Pulang</b> : <br>
                                        @if ($datas->waktu_pulang == null)
                                        @else 
                                        {{date('d-m-Y H:i', strtotime($datas->waktu_pulang))}}
                                        @endif
                                        <br>
                                        {{-- <b>Jumlah Jam</b> : {{$datas->jumlah_jam}}</td> --}}
                                    <td>
                                        <b>Lokasi Kerja</b> : <br>{{$datas->lokasi_masuk}} <br>
                                        <b>Lihat Map</b> : <br><button onclick="bukaMap('{{$datas->titik_masuk}}')" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lokasi Absen">Lihat Lokasi</button> <br>
                                        <b>No Kendaraan</b> : {{$datas->no_ct_masuk}} <br>
                                        <b>HM Awal</b> : <br>{{$datas->hm_awal}} <br>
                                        <b>HM Akhir</b> : <br>{{$datas->hm_akhir}}
                                    </td>
                                    <td> 
                                        <b>Masuk</b> : <br>{{$datas->catatan_masuk}} <br>
                                        <b>Pulang</b> : <br>{{$datas->catatan_pulang}}
                                    </td>
                                    {{-- <td>
                                        @php
                                            $cekAbsen = DB::table('absens')
                                                ->where('kondisi','!=','Absen')
                                                ->where('id_pegawai',$datas->id)
                                                ->orderBy('waktu_masuk','DESC')
                                                ->first();
                                        @endphp
                                        @if ($cekAbsen)
                                            @if ($date >= $cekAbsen->waktu_masuk && $date <= $cekAbsen->waktu_pulang)
                                                <b>Status</b> : <br> <span class="badge badge-info">{{$cekAbsen->kondisi}}</span>
                                            @endif
                                        @else
                                        <b>Masuk</b> : <br>
                                        @if ($datas->waktu_masuk)
                                        <span class="badge badge-success">Terkirim</span>
                                        @else
                                        <span class="badge badge-warning">Belum Absen</span>
                                        @endif
                                        <br>
                                        <b>Pulang</b> : <br>
                                        @if ($datas->waktu_pulang)
                                        <span class="badge badge-success">Terkirim</span>
                                        @else
                                        <span class="badge badge-warning">Belum Absen</span>
                                        @endif
                                        @endif
                                    </td> --}}
                                    <td>
                                        <b>Selfie</b> : <br>
                                        <img src="{{$datas->selfie_masuk}}" width="100"> <br>
                                        <b>Timesheet</b> : <br>
                                        <img src="{{$datas->timesheet_masuk}}" width="100">
                                    </td>
                                    <td>
                                        <b>Selfie</b> : <br>
                                        <img src="{{$datas->selfie_pulang}}" width="100"> <br>
                                        <b>Timesheet</b> : <br>
                                        <img src="{{$datas->timesheet_pulang}}" width="100">
                                    </td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>

                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        <!-- end row-->

    </div> <!-- container-fluid -->

</div> <!-- content -->
@include('rekap.map')
@endsection

@section('js')
    <!-- datatable js -->
    <script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    
    <script src="{{ asset('assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('assets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/dataTables.select.min.js') }}"></script>

    <!-- Datatables init -->
    <script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>

    <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/leaflet/leaflet.js') }}"></script>

    <script type="text/javascript">
        var table = $('#data-pegawai').DataTable();

        function addForm() {
          save_method = "add";
          $('input[name=_method]').val('POST');
          $('#modal-form').modal('show');
          $('.modal-title').text('Tambah Data Pegawai');
          $('#modal-form form')[0].reset();
        }
        
        function bukaMap(titik_lokasi) {
            $('#modal-form').modal('show');
            $('.modal-title').text('Lokasi Absen Pegawai');
            let lokasi = titik_lokasi.split(",");
          const map = L.map('map').setView([lokasi[0], lokasi[1]], 13);
          const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
          }).addTo(map);
        L.marker([lokasi[0], lokasi[1]]).addTo(map);

        }
    
        function deleteData(id){
          var csrf_token = $('meta[name="csrf-token"]').attr('content');
          swal({
              title: 'Apa anda yakin?',
              text: "Anda tidak dapat membatalkan perintah ini!",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yakin!'
          }).then(function () {
              $.ajax({
                  url : "{{ url('pegawai/delete') }}" + '/' + id,
                  type : "GET",
                  data : {'_method' : 'GET', '_token' : csrf_token},
                  success : function(data) {
                    window.location.reload();
                      swal({
                          title: 'Success!',
                          text: data.message,
                          icon: 'success',
                          timer: '1500'
                      })
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '1500'
                      })
                  }
              });
          });
        }

    
        $(function(){
              $('#modal-form').on('submit', function (e) {
                  if (!e.isDefaultPrevented()){
                      var id = $('#id').val();
                      if (save_method == 'add') url = "{{ url('pegawai/save/') }}";
                      else url = "{{ url('pegawai/update') . '/' }}" + id;
    
                      $.ajax({
                          url : url,
                          type : "POST",
                          //data : $('#modal-form form').serialize(),
                          data : new FormData($('#modal-form form')[0]),
                          contentType : false,
                          processData : false,
                          success : function(data) {
                              $('#modal-form').modal('hide');
                              swal({
                                  title: 'Success!',
                                  text: data.message,
                                  icon: 'success',
                                  timer: '1500'
                                })
                            window.location.reload();
                          },
                          error : function(){
                              swal({
                                  title: 'Oops...',
                                  text: data.message,
                                  icon: 'error',
                                  timer: '1500'
                              })
                          }
                      });
                      return false;
                  }
              });
          });
    </script>
@endsection