<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            [
                'id' => Str::upper(Str::uuid()),
                'name' => 'Administrator',
                'email' => 'admin',
                'password' => bcrypt('admin321'),
                'role' => 'Admin'
            ]
            ));
    }
}
