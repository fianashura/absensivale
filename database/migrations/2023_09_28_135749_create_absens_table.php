<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absens', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('id_pegawai',36);
            $table->string('shift',10);
            // $table->string('jenis_absen',10);
            $table->date('waktu_masuk');
            $table->date('waktu_pulang');
            $table->integer('jumlah_jam');
            $table->string('lokasi_masuk',100);
            $table->string('lokasi_pulang',100);
            $table->string('titik_masuk',100);
            $table->string('titik_pulang',100);
            $table->string('no_ct_masuk',20);
            $table->string('no_ct_pulang',20);
            $table->string('hm_awal',20);
            $table->string('hm_akhir',20);
            $table->string('catatan_masuk',100);
            $table->string('catatan_pulang',100);
            $table->string('selfie_masuk',100);
            $table->string('selfie_pulang',100);
            $table->string('timesheet_masuk',100);
            $table->string('timesheet_pulang',100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absens');
    }
}
