<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absensis', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('id_pegawai',36);
            $table->string('shift',10);
            $table->string('jenis_absen',10);
            $table->date('waktu_absen');
            $table->integer('jumlah_jam');
            $table->string('lokasi_kerja',100);
            $table->string('titik_lokasi',100);
            $table->string('no_ct',20);
            $table->string('hm_awal',20);
            $table->string('hm_akhir',20);
            $table->string('catatan',100);
            $table->string('selfie',100);
            $table->string('timesheet',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absensis');
    }
}
