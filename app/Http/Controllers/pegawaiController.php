<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\pegawai;
use App\User;
use Illuminate\Support\Str;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PegawaiImport;

class pegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = DB::table('pegawais')->get();
        $no = 0;
        return view('pegawai.index', compact('data','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    public function import(Request $request) 
    {
        Excel::import(new PegawaiImport,$request->file('file'));
           
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_karyawan = Str::upper(Str::uuid());
        $karyawan = new pegawai;
        $karyawan->id = $id_karyawan;
        $karyawan->id_badge = $request['id_badge'];
        $karyawan->nama = $request['nama'];
        $karyawan->posisi = $request['posisi'];

        $karyawan->save();

        $user = new User;
        $user->id = Str::upper(Str::uuid());
        $user->id_pegawai = $id_karyawan;
        $user->role = 'Pegawai';
        $user->name = $request['nama'];
        $user->email = $request['id_badge'];
        $user->password = bcrypt($request['password']);
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Terinput'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pegawai = pegawai::findOrFail($id);
        // dd($pegawai);
        return $pegawai;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $karyawan = new pegawai;
        // $karyawan->id_badge = $request['id_badge'];
        // $karyawan->nama = $request['nama'];
        // $karyawan->posisi = $request['posisi'];

        // $karyawan->save();
        DB::table('pegawais')->where('id',$id)->update([
            'id_badge' => $request->id_badge,
            'nama' => $request->nama,
            'posisi' => $request->posisi,
        ]);
        if($request->password){
            DB::table('users')->where('id_pegawai',$id)->update([
                'email' => $request->id_badge,
                'name' => $request->nama,
                'password' => bcrypt($request->password),
            ]);
        } else {
            DB::table('users')->where('id_pegawai',$id)->update([
                'email' => $request->id_badge,
                'name' => $request->nama,
            ]);
        }

        // dd($updatePegawai);

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('pegawais')->where('id',$id)->delete();
        DB::table('users')->where('id_pegawai',$id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }
}
