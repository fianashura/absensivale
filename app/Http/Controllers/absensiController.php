<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
// use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\pegawai;
use App\absensi;
use App\absen;
use App\User;
use Illuminate\Support\Str;
use DB;
use Auth;
Use Alert;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image;
use App\Exports\AbsensiExport;

class absensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pegawai = DB::table('pegawais')->where('id', Auth::user()->id_pegawai)->first();
        $mobile = 'Desktop';
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
            // dd('using mobile');
            $mobile = 'Mobile';
        }


        return view('absensi.index', compact('pegawai','mobile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function export($tanggal)
	{
		return Excel::download(new AbsensiExport($tanggal), 'absensi.xlsx');
	}

    public function downloadPictures($tanggal)
    {
        // Retrieve the pictures from the database
        $data = DB::table('pegawais')
            ->leftJoin(DB::raw("(SELECT * FROM absens WHERE  DATE_FORMAT(waktu_masuk,'%Y-%m-%d') = '".$tanggal."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('pegawais.*', 'absen.shift','absen.kondisi','absen.waktu_masuk', 'absen.waktu_pulang', 'absen.lokasi_masuk', 'absen.lokasi_pulang', 'absen.titik_masuk', 'absen.titik_pulang','absen.no_ct_masuk', 'absen.no_ct_pulang','absen.hm_awal','absen.hm_akhir','absen.selfie_masuk', 'absen.selfie_pulang','absen.timesheet_masuk', 'absen.timesheet_pulang', 'absen.jumlah_jam', 'absen.catatan_masuk', 'absen.catatan_pulang')
            ->get();

        // $fileSelfieMasuk = [];
        // $fileSelfiePulang = [];
        // $fileTimesheetMasuk = [];
        // $fileTimesheetPulang = [];
        // foreach ($data as $datas) {
        //     $fileSelfieMasuk[$datas->id] = public_path().'/foto/absensi/'.$datas->selfie_masuk;
        // }
        // Create a ZIP archive to store the images
        $zip = new \ZipArchive;
        $zipFileName = "rekap-absen-{$tanggal}.zip";
        $zip->open($zipFileName, \ZipArchive::CREATE);

        // Loop through the pictures and add them to the ZIP archive
        foreach ($data as $datas) {
            // Decode the base64 string to binary data
            if($datas->selfie_masuk) {
                // $dataSelfieMasuk = base64_decode($datas->selfie_masuk);
                // dd($dataSelfieMasuk);
                $zip->addFile(public_path().'/foto/absensi/'.$datas->selfie_masuk, $datas->selfie_masuk);
            };
            if($datas->selfie_pulang) {
                // $dataSelfiePulang = base64_decode($datas->selfie_pulang);
                $zip->addFile(public_path().'/foto/absensi/'.$datas->selfie_pulang, $datas->selfie_pulang);
            };

            if($datas->timesheet_masuk) {
                // $dataTimesheetMasuk = base64_decode($datas->timesheet_masuk);
                $zip->addFile(public_path().'/foto/absensi/'.$datas->timesheet_masuk, $datas->timesheet_masuk);

            };
            if($datas->timesheet_pulang) {
                // $dataTimesheetPulang = base64_decode($datas->timesheet_pulang);
                $zip->addFile(public_path().'/foto/absensi/'.$datas->timesheet_pulang, $datas->timesheet_pulang);
            };

            // Add the image to the ZIP archive with a unique name
        }

        // Close the ZIP archive
        $zip->close();

        // Create a response with the ZIP file
        return response()->download($zipFileName)->deleteFileAfterSend(true);
    }

    public function cekAbsensi()
    {
        //
    }
    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $id_absensi = Str::upper(Str::uuid());
        $mytime = Carbon::now('Asia/Makassar');
        $date = $mytime->toDateTimeString();
        $format = "d/m/Y, H.i.s";
        $absensi = new absensi;
        $absensi->id = $id_absensi;
        $absensi->id_pegawai = Auth::user()->id_pegawai;
        $absensi->kondisi = $request['kondisi'];
        $absensi->shift = $request['shift'];
        if ($request['kondisi'] == 'Absen') {
            if ($request['absen'] == 'Masuk') {
                $carbonDate = Carbon::createFromFormat($format, $request['waktu_mulai']);
                $waktu_absen = $carbonDate->format('Y-m-d H:i:s');
            } else {
                $carbonDate = Carbon::createFromFormat($format, $request['waktu_selesai']);
                $waktu_absen = $carbonDate->format('Y-m-d H:i:s');
            }
            $absensi->jenis_absen = $request['absen'];
            $waktu_kembali = null;
        } else {
            $absensi->jenis_absen = 'Masuk';
            $waktu_absen = $request['waktu_mulai'];
            $waktu_kembali = $request['waktu_selesai'];
        }
        // dd($waktu_absen);
        $absensi->waktu_absen = $waktu_absen;
        $absensi->waktu_kembali = $waktu_kembali;
        $absensi->jumlah_jam = $request['jumlah_jam'];
        $absensi->lokasi_kerja = $request['lokasi_kerja'];
        $absensi->titik_lokasi = $request['titik_map'];
        $absensi->no_ct = $request['no_ct'];
        $absensi->hm_awal = $request['hm_awal'];
        $absensi->hm_akhir = $request['hm_akhir'];
        $absensi->catatan = $request['catatan'];

        $path = public_path().'/foto/absensi';
        
        $data_selfie_url = null;
        $data_timesheet_url = null;
        if($request['device'] == 'Mobile') {
            $fileSelfie = $request->file('selfieFile');
            $resizedImageSelfie = Image::make($fileSelfie)->resize(200, 300)->encode('jpg');
            $imageSelfie = base64_encode($resizedImageSelfie); 
            $data_selfie_url = 'data:image/jpeg;base64,'. $imageSelfie ;
            // $data_selfie_url = 'data:image/jpeg;base64,'. $resizedImageSelfie ;
            
            if($request->file('timesheetFile')) {
                $fileTimesheet = $request->file('timesheetFile');
                $resizedImageTimesheet = Image::make($fileTimesheet)->resize(200, 300)->encode('jpg');
                $imageTimesheet = base64_encode($resizedImageTimesheet); 
                $data_timesheet_url = 'data:image/jpeg;base64,'. $imageTimesheet ;
            }
        } else {
            $data_selfie_url = $request['selfie'];
            if($request['timesheet']) {
                $data_timesheet_url = $request['timesheet'];
            }
        }

        $absensi->selfie = $data_selfie_url;
        $absensi->timesheet = $data_timesheet_url;
        // dd($imageSelfie);
        $absensi->save();
        

        // dd($request);
        // alert()->success('Selamat', 'Data Anda Berhasil Tersimpan');
        // return redirect()->route('logout');
        return response()->json([
            'success' => true,
            'message' => 'Absen Berhasil'
        ]);
    }
    
    public function storeAbsen(Request $request)
    {
        // dd($request);
        $dateAbsen = date('Y-m-d');
        $cekAbsen = DB::table('absens')
            ->where('id_pegawai', Auth::user()->id_pegawai)
            ->where(DB::raw("DATE_FORMAT(waktu_masuk,'%Y-%m-%d')"), $dateAbsen)
            ->first();
        $id_absensi = Str::upper(Str::uuid());
        $mytime = Carbon::now('Asia/Makassar');
        $date = $mytime->toDateTimeString();
        $format = "d/m/Y, H.i.s";

        $data_selfie_url = null;
        $data_timesheet_url = null;
        $selfi_name = null;
        $timesheet_name = null;
        if($request['device'] == 'Mobile') {
            $fileSelfie = $request->file('selfieFile');
            $resizedImageSelfie = Image::make($fileSelfie)->resize(200, 300)->encode('jpg');
            $imageSelfie = base64_encode($resizedImageSelfie); 
            $data_selfie_url = 'data:image/jpeg;base64,'. $imageSelfie;
            $data_selfie_url = str_replace('data:image/jpeg;base64,', '', $data_selfie_url);
            $data_selfie_url = str_replace(' ', '+', $data_selfie_url);
            $selfi_name = 'Selfie-'.$request['kondisi'].'-'.$request['absen'].'-'.Auth::user()->name.'-'.$date.'.png';
            \File::put(public_path(). '/foto/absensi/' . $selfi_name, base64_decode($data_selfie_url));
            // $data_selfie_url = 'data:image/jpeg;base64,'. $resizedImageSelfie ;
            
            if($request->file('timesheetFile')) {
                $fileTimesheet = $request->file('timesheetFile');
                $resizedImageTimesheet = Image::make($fileTimesheet)->resize(200, 300)->encode('jpg');
                $imageTimesheet = base64_encode($resizedImageTimesheet); 
                $data_timesheet_url = 'data:image/jpeg;base64,'. $imageTimesheet ;
                $data_timesheet_url = str_replace('data:image/jpeg;base64,', '', $data_timesheet_url);
                $data_timesheet_url = str_replace(' ', '+', $data_timesheet_url);
                $timesheet_name = 'Timesheet-'.$request['kondisi'].'-'.$request['absen'].'-'.Auth::user()->name.'-'.$date.'.png';
                \File::put(public_path(). '/foto/absensi/' . $timesheet_name, base64_decode($data_timesheet_url));
            }
        } else {
            $data_selfie_url = $request['selfie'];
            $data_selfie_url = str_replace('data:image/jpeg;base64,', '', $data_selfie_url);
            $data_selfie_url = str_replace(' ', '+', $data_selfie_url);
            $selfi_name = 'Selfie-'.$request['kondisi'].'-'.$request['absen'].'-'.Auth::user()->name.'-'.$date.'.png';
            \File::put(public_path(). '/foto/absensi/' . $selfi_name, base64_decode($data_selfie_url));

            if($request['timesheet']) {
                $data_timesheet_url = $request['timesheet'];
                $data_timesheet_url = str_replace('data:image/jpeg;base64,', '', $data_timesheet_url);
                $data_timesheet_url = str_replace(' ', '+', $data_timesheet_url);
                $timesheet_name = 'Timesheet-'.$request['kondisi'].'-'.$request['absen'].'-'.Auth::user()->name.'-'.$date.'.png';
                \File::put(public_path(). '/foto/absensi/' . $timesheet_name, base64_decode($data_timesheet_url));
            }
        }
        
        if ($request['kondisi'] != 'Absen') {
            $absensi = new absen;
            $absensi->id = $id_absensi;
            $absensi->id_pegawai = Auth::user()->id_pegawai;
            $absensi->kondisi = $request['kondisi'];
            $absensi->shift = $request['shift'];
            $absensi->waktu_masuk = $request['waktu_mulai'];
            $absensi->waktu_pulang = $request['waktu_selesai'];
            $absensi->catatan_masuk = $request['catatan'];
            $absensi->selfie_masuk = $selfi_name;
            $absensi->save();
        } else {
            if ($request['absen'] == 'Masuk') {
                $absensi = new absen;
                $absensi->id = $id_absensi;
                $absensi->id_pegawai = Auth::user()->id_pegawai;
                $absensi->kondisi = $request['kondisi'];
                $absensi->shift = $request['shift'];
                
                $carbonDate = Carbon::createFromFormat($format, $request['waktu_mulai']);
                $absensi->waktu_masuk = $carbonDate->format('Y-m-d H:i:s');
                
                $absensi->lokasi_masuk = $request['lokasi_kerja'];
                $absensi->titik_masuk = $request['titik_map'];
                $absensi->no_ct_masuk = $request['no_ct'];
                $absensi->hm_awal = $request['hm_awal'];
                $absensi->catatan_masuk = $request['catatan'];
        
                $absensi->selfie_masuk = $selfi_name;
                $absensi->timesheet_masuk = $timesheet_name;
                // dd($imageSelfie);
                $absensi->save();
            } else {
                if ($cekAbsen->waktu_pulang == null) {
                    $carbonDate = Carbon::createFromFormat($format, $request['waktu_selesai']);
                    $waktu_pulang = $carbonDate->format('Y-m-d H:i:s');

                    $absensi = DB::table('absens')->where('id',$cekAbsen->id)->update([
                        'waktu_pulang' => $waktu_pulang,
                        'lokasi_pulang' => $request->lokasi_kerja,
                        'titik_pulang' => $request->titik_map,
                        'no_ct_pulang' => $request->no_ct,
                        'hm_akhir' => $request->hm_akhir,
                        'catatan_pulang' => $request->catatan,
                        'selfie_pulang' => $selfi_name,
                        'timesheet_pulang' => $timesheet_name,
                    ]);
                }
            }

        
        }
        
        return response()->json([
            'success' => true,
            'message' => 'Absen Berhasil'
        ]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function absenLogout() 
    {
        Auth::logout();
        return redirect('login');
    }

    public function cekAbsen($waktu)
    {
        // $mytime = Carbon::now('Asia/Makassar');
        $date = date('Y-m-d');
        // if($waktu == 'Masuk'){
            // $date = date('Y-m-d');
            // dd(Auth::user()->id_pegawai, $waktu, $date);
            $absen = DB::table('absens')
              ->where('id_pegawai', Auth::user()->id_pegawai)
            //   ->where('jenis_absen', $waktu)
              ->orderBy('waktu_masuk','desc')
            //   ->where(DB::raw("DATE_FORMAT(waktu_masuk,'%Y-%m-%d')"), $date)
              ->first();
            //   if ($absen)
            //   dd($absen->waktu_masuk);
            //   dd($date);
            if ($absen) {
                if ($date > $absen->waktu_masuk) {
                    $absensi = 'Belum';
                } else {
                    $absensi = 'Sudah';
                }
            } else {
                $absensi = 'Belum';
            }
            
            return response()->json([
                'success' => true,
                'message' => 'Data Absen',
                'data' => $absen,
                'absensi' => $absensi,
            ]);

    }
    
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('absens')->where('id',$id)->delete();
        // DB::table('users')->where('id_pegawai',$id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Tehapus'
        ]);
    }
    
}
