<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\pegawai;
use App\User;
use Illuminate\Support\Str;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image;
use App\Exports\AbsensiPegawaiExport;
use App\Exports\AbsensiPeriodeExport;

class rekapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = DB::table('pegawais')->get();
        $date = date('Y-m-d');
        $data = DB::table('pegawais')
            ->leftJoin(DB::raw("(SELECT * FROM absensis WHERE jenis_absen = 'Masuk' and DATE_FORMAT(waktu_absen,'%Y-%m-%d') = '".$date."') as masuk"), 'masuk.id_pegawai', '=', 'pegawais.id')
            ->leftJoin(DB::raw("(SELECT * FROM absensis WHERE jenis_absen = 'Pulang' and DATE_FORMAT(waktu_absen,'%Y-%m-%d') = '".$date."') as keluar"), 'keluar.id_pegawai', '=', 'pegawais.id')
            ->select('pegawais.*', 'masuk.shift','masuk.kondisi','masuk.waktu_absen as absen_masuk', 'masuk.waktu_kembali', 'masuk.lokasi_kerja','masuk.titik_lokasi','masuk.no_ct','masuk.hm_awal','keluar.hm_akhir','masuk.catatan','masuk.selfie','masuk.timesheet', 'keluar.waktu_absen as absen_pulang', 'keluar.jumlah_jam', 'keluar.catatan as catatan_pulang', 'keluar.selfie as selfie_pulang', 'keluar.timesheet as timesheet_pulang')
            ->get();
        $no = 0;
        return view('rekap.index', compact('data','no', 'date'));
    }
    
    public function indexRekap()
    {
        // $data = DB::table('pegawais')->get();
        $date = date('Y-m-d');
        $data = DB::table('pegawais')
            ->leftJoin(DB::raw("(SELECT * FROM absens WHERE  DATE_FORMAT(waktu_masuk,'%Y-%m-%d') = '".$date."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('pegawais.*', 'absen.id as id_absen', 'absen.shift','absen.kondisi','absen.waktu_masuk', 'absen.waktu_pulang', 'absen.lokasi_masuk', 'absen.lokasi_pulang', 'absen.titik_masuk', 'absen.titik_pulang','absen.no_ct_masuk', 'absen.no_ct_pulang','absen.hm_awal','absen.hm_akhir','absen.selfie_masuk', 'absen.selfie_pulang','absen.timesheet_masuk', 'absen.timesheet_pulang', 'absen.jumlah_jam', 'absen.catatan_masuk', 'absen.catatan_pulang')
            ->get();
        // dd($data);
        $no = 0;
        return view('rekap.index', compact('data','no', 'date'));
    }
    
    public function dateRekap(Request $request)
    {
        
        // $data = DB::table('pegawais')->get();
        $date = $request->date;
        $data = DB::table('pegawais')
            ->leftJoin(DB::raw("(SELECT * FROM absens WHERE  DATE_FORMAT(waktu_masuk,'%Y-%m-%d') = '".$date."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('pegawais.*', 'absen.id as id_absen', 'absen.shift','absen.kondisi','absen.waktu_masuk', 'absen.waktu_pulang', 'absen.lokasi_masuk', 'absen.lokasi_pulang', 'absen.titik_masuk', 'absen.titik_pulang','absen.no_ct_masuk', 'absen.no_ct_pulang','absen.hm_awal','absen.hm_akhir','absen.selfie_masuk', 'absen.selfie_pulang','absen.timesheet_masuk', 'absen.timesheet_pulang', 'absen.jumlah_jam', 'absen.catatan_masuk', 'absen.catatan_pulang')
            ->get();
        $no = 0;
        return view('rekap.index', compact('data','no', 'date'));
    }
    
    public function rekapPeriode()
    {
        // $data = DB::table('pegawais')->get();
        $from = date('Y-m-d');
        $to = date('Y-m-d');
        $data = DB::table('pegawais')
            ->leftJoin(DB::raw("(SELECT * FROM absens WHERE DATE_FORMAT(waktu_masuk,'%Y-%m-%d') between '".$from."' and '".$to."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('pegawais.*', 'absen.shift','absen.kondisi','absen.waktu_masuk', 'absen.waktu_pulang', 'absen.lokasi_masuk', 'absen.lokasi_pulang', 'absen.titik_masuk', 'absen.titik_pulang','absen.no_ct_masuk', 'absen.no_ct_pulang','absen.hm_awal','absen.hm_akhir','absen.selfie_masuk', 'absen.selfie_pulang','absen.timesheet_masuk', 'absen.timesheet_pulang', 'absen.jumlah_jam', 'absen.catatan_masuk', 'absen.catatan_pulang')
            ->get();
        $no = 0;
        return view('rekap.rekapPeriode', compact('data','no', 'from', 'to'));
    }
    
    public function dateRekapPeriode(Request $request)
    {
        
        // $data = DB::table('pegawais')->get();
        $from = $request->from;
        $to = $request->to;
        $data = DB::table('pegawais')
           ->leftJoin(DB::raw("(SELECT * FROM absens WHERE DATE_FORMAT(waktu_masuk,'%Y-%m-%d') between '".$from."' and '".$to."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('pegawais.*', 'absen.shift','absen.kondisi','absen.waktu_masuk', 'absen.waktu_pulang', 'absen.lokasi_masuk', 'absen.lokasi_pulang', 'absen.titik_masuk', 'absen.titik_pulang','absen.no_ct_masuk', 'absen.no_ct_pulang','absen.hm_awal','absen.hm_akhir','absen.selfie_masuk', 'absen.selfie_pulang','absen.timesheet_masuk', 'absen.timesheet_pulang', 'absen.jumlah_jam', 'absen.catatan_masuk', 'absen.catatan_pulang')
            ->get();
        $no = 0;
        return view('rekap.rekapPeriode', compact('data','no', 'from', 'to'));
    }
    
    public function rekapByIdPegawai($id)
    {
        // $data = DB::table('pegawais')->get();
        $month = $date = date('m');
        if ($month == '1') {
            $bulan = 'Januari';
        } else if ($month == '2') {
            $bulan = 'Februari';
        } else if ($month == '3') {
            $bulan = 'Maret';
        } else if ($month == '4') {
            $bulan = 'April';
        } else if ($month == '5') {
            $bulan = 'Mei';
        } else if ($month == '6') {
            $bulan = 'Juni';
        } else if ($month == '7') {
            $bulan = 'Juli';
        } else if ($month == '8') {
            $bulan = 'Agustus';
        } else if ($month == '9') {
            $bulan = 'September';
        } else if ($month == '10') {
            $bulan = 'Oktober';
        } else if ($month == '11') {
            $bulan = 'November';
        } else if ($month == '12') {
            $bulan = 'Desember';
        }

        $data = DB::table('absens')
            // ->leftJoin(DB::raw("(SELECT * FROM absens WHERE MONTH(waktu_masuk) = '".$month."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('id_pegawai','shift','kondisi','waktu_masuk', 'waktu_pulang', 'lokasi_masuk', 'lokasi_pulang', 'titik_masuk', 'titik_pulang','no_ct_masuk', 'no_ct_pulang','hm_awal','hm_akhir','selfie_masuk', 'selfie_pulang','timesheet_masuk', 'timesheet_pulang', 'jumlah_jam', 'catatan_masuk', 'catatan_pulang')
            ->whereMonth('waktu_masuk','=',$month)
            ->where('id_pegawai', $id)
            ->orderBy('waktu_masuk', 'asc')
            ->get();
        $no = 0;
        return view('rekap.rekapPegawai', compact('data','no', 'month', 'id', 'bulan'));
    }
    
    public function dateRekapByIdPegawai(Request $request, $id)
    {
        // $data = DB::table('pegawais')->get();
        $month = $request->month;
        if ($month == '1') {
            $bulan = 'Januari';
        } else if ($month == '2') {
            $bulan = 'Februari';
        } else if ($month == '3') {
            $bulan = 'Maret';
        } else if ($month == '4') {
            $bulan = 'April';
        } else if ($month == '5') {
            $bulan = 'Mei';
        } else if ($month == '6') {
            $bulan = 'Juni';
        } else if ($month == '7') {
            $bulan = 'Juli';
        } else if ($month == '8') {
            $bulan = 'Agustus';
        } else if ($month == '9') {
            $bulan = 'September';
        } else if ($month == '10') {
            $bulan = 'Oktober';
        } else if ($month == '11') {
            $bulan = 'November';
        } else if ($month == '12') {
            $bulan = 'Desember';
        }
        $data = DB::table('absens')
            // ->leftJoin(DB::raw("(SELECT * FROM absens WHERE MONTH(waktu_masuk) = '".$month."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('id_pegawai','shift','kondisi','waktu_masuk', 'waktu_pulang', 'lokasi_masuk', 'lokasi_pulang', 'titik_masuk', 'titik_pulang','no_ct_masuk', 'no_ct_pulang','hm_awal','hm_akhir','selfie_masuk', 'selfie_pulang','timesheet_masuk', 'timesheet_pulang', 'jumlah_jam', 'catatan_masuk', 'catatan_pulang')
            ->whereMonth('waktu_masuk','=',$month)
            ->where('id_pegawai', $id)
            ->orderBy('waktu_masuk', 'asc')
            ->get();
        $no = 0;
        return view('rekap.rekapPegawai', compact('data','no', 'month', 'id', 'bulan'));
    }

    public function exportPegawai($month, $id)
	{
		return Excel::download(new AbsensiPegawaiExport($month, $id), 'absensiPegawai.xlsx');
	}
    
    public function exportRekapPeriode($from, $to)
	{
		return Excel::download(new AbsensiPeriodeExport($from, $to), 'absensiRekapPeriode.xlsx');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeToImage($from, $to)
    {
        $data = DB::table('pegawais')
           ->leftJoin(DB::raw("(SELECT * FROM absens WHERE DATE_FORMAT(waktu_masuk,'%Y-%m-%d') between '".$from."' and '".$to."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('pegawais.*','absen.id as id_absen', 'absen.shift','absen.kondisi','absen.waktu_masuk', 'absen.waktu_pulang', 'absen.lokasi_masuk', 'absen.lokasi_pulang', 'absen.titik_masuk', 'absen.titik_pulang','absen.no_ct_masuk', 'absen.no_ct_pulang','absen.hm_awal','absen.hm_akhir','absen.selfie_masuk', 'absen.selfie_pulang','absen.timesheet_masuk', 'absen.timesheet_pulang', 'absen.jumlah_jam', 'absen.catatan_masuk', 'absen.catatan_pulang')
            ->get();
        foreach ($data as $datas) {
            // echo($datas->shift);
            $imageSelfieMasukName = null;
            $imageSelfiePulangName = null;
            $imageTimesheetMasukName = null;
            $imageTimesheetPulangName = null;
            if ($datas->waktu_masuk && $datas->selfie_masuk) {
                $imageSelfieMasuk = $datas->selfie_masuk;
                $imageSelfieMasuk = str_replace('data:image/jpeg;base64,', '', $imageSelfieMasuk);
                $imageSelfieMasuk = str_replace(' ', '+', $imageSelfieMasuk);
                $imageSelfieMasukName = 'Selfie-Absen-Masuk-'.$datas->nama.'-'.$datas->waktu_masuk.'.png';
                \File::put(public_path(). '/foto/absensi/' . $imageSelfieMasukName, base64_decode($imageSelfieMasuk));
            }

            if ($datas->waktu_pulang && $datas->selfie_pulang) {
                $imageSelfiePulang = $datas->selfie_pulang;
                $imageSelfiePulang = str_replace('data:image/jpeg;base64,', '', $imageSelfiePulang);
                $imageSelfiePulang = str_replace(' ', '+', $imageSelfiePulang);
                $imageSelfiePulangName = 'Selfie-Absen-Pulang-'.$datas->nama.'-'.$datas->waktu_pulang.'.png';
                \File::put(public_path(). '/foto/absensi/' . $imageSelfiePulangName, base64_decode($imageSelfiePulang));
            }
            
            if ($datas->waktu_masuk && $datas->timesheet_masuk) {
                $imageTimesheetMasuk = $datas->timesheet_masuk;
                $imageTimesheetMasuk = str_replace('data:image/jpeg;base64,', '', $imageTimesheetMasuk);
                $imageTimesheetMasuk = str_replace(' ', '+', $imageTimesheetMasuk);
                $imageTimesheetMasukName = 'Timesheet-Absen-Masuk-'.$datas->nama.'-'.$datas->waktu_masuk.'.png';
                \File::put(public_path(). '/foto/absensi/' . $imageTimesheetMasukName, base64_decode($imageTimesheetMasuk));
            }

            if ($datas->waktu_pulang && $datas->timesheet_pulang) {
                $imageTimesheetPulang = $datas->timesheet_pulang;
                $imageTimesheetPulang = str_replace('data:image/jpeg;base64,', '', $imageTimesheetPulang);
                $imageTimesheetPulang = str_replace(' ', '+', $imageTimesheetPulang);
                $imageTimesheetPulangName = 'Timesheet-Absen-Pulang-'.$datas->nama.'-'.$datas->waktu_pulang.'.png';
                \File::put(public_path(). '/foto/absensi/' . $imageTimesheetPulangName, base64_decode($imageTimesheetPulang));
            }

            DB::table('absens')->where('id',$datas->id_absen)->update([
                'selfie_masuk' => $imageSelfieMasukName,
                'selfie_pulang' => $imageSelfiePulangName,
                'timesheet_masuk' => $imageTimesheetMasukName,
                'timesheet_pulang' => $imageTimesheetPulangName,
            ]);
        }
    }
    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('absens')->where('id',$id)->delete();
        // DB::table('users')->where('id_pegawai',$id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Tehapus'
        ]);
    }
}
