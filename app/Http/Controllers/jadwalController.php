<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\pegawai;
use App\jadwal;
use App\User;
use Illuminate\Support\Str;
use DB;

class jadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = DB::table('jadwals')->get();
        $no = 0;
        return view('jadwal.index', compact('data','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_jadwal = Str::upper(Str::uuid());
        $jadwal = new jadwal;
        $jadwal->id = $id_jadwal;
        $jadwal->judul = $request['judul'];

        $file  = $request->file('foto');
        $exe = $file->getClientOriginalExtension();
        $filename = rand(1,100000).'.'.$exe;
        $request->file('foto')->move("foto/jadwal", $filename);

        $jadwal->lampiran = $filename;

        $jadwal->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Terinput'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jadwal = jadwal::where('id',$id)->first();
        // dd($jadwal);
        return response()->json([
            'data' => $jadwal,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->lampiran != NULL){
            $file  = $request->file('foto');
            $exe = $file->getClientOriginalExtension();
            $filename = rand(1,100000).'.'.$exe;
            $request->file('foto')->move("foto/jadwal", $filename);

            DB::table('jadwals')->where('id',$id)->update([
                'judul' => $request->judul,
                'lampiran' => $filename,
            ]);
        }else{
            DB::table('jadwals')->where('id',$id)->update([
                'judul' => $request->judul,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('jadwals')->where('id',$id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Terupdate'
        ]);
    }
}
