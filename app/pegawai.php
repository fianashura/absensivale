<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pegawai extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pegawais';
    public $incrementing = false;
    protected $fillable = ['id', 'id_badge', 'nama', 'posisi'];
}
