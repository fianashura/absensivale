<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'histories';
    public $incrementing = false;
    protected $fillable = [];
}
