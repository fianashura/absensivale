<?php

namespace App\Exports;

use App\absens;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AbsensiPeriodeExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function  __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function collection()
    {
        $date = date('Y-m-d');
        $data = DB::table('pegawais')
            ->leftJoin(DB::raw("(SELECT * FROM absens WHERE DATE_FORMAT(waktu_masuk,'%Y-%m-%d') between '".$this->from."' and '".$this->to."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('pegawais.id_badge', 'pegawais.nama', 'absen.shift','absen.kondisi','absen.waktu_masuk', 'absen.waktu_pulang', 'absen.lokasi_masuk', 'absen.lokasi_pulang', 'absen.titik_masuk', 'absen.titik_pulang','absen.no_ct_masuk','absen.hm_awal','absen.hm_akhir', 'absen.catatan_masuk', 'absen.catatan_pulang')
            ->get();
        // dd($data);
        return $data;
    }

    public function headings(): array
    {
        return ["ID Badge", "Nama", "Shift", "Kondisi", "Masuk", "Pulang", "Lokasi Masuk", "Lokasi Pulang", "Titik Masuk", "Titik Pulang", "No Kendaraan", "HM Awal", "HM Akhir", "Catatan Masuk", "Catatan Pulang"];
    }
}
