<?php

namespace App\Exports;

use App\absens;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AbsensiPegawaiExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function  __construct($month, $id)
    {
        $this->month= $month;
        $this->id= $id;
    }

    public function collection()
    {
        $date = date('Y-m-d');
        $data = DB::table('absens')
            // ->leftJoin(DB::raw("(SELECT * FROM absens WHERE MONTH(waktu_masuk) = '".$month."') as absen"), 'absen.id_pegawai', '=', 'pegawais.id')
            ->select('shift','kondisi','waktu_masuk', 'waktu_pulang', 'lokasi_masuk', 'lokasi_pulang', 'titik_masuk', 'titik_pulang','no_ct_masuk', 'no_ct_pulang','hm_awal','hm_akhir','catatan_masuk', 'catatan_pulang')
            ->whereMonth('waktu_masuk','=',$this->month)
            ->where('id_pegawai', $this->id)
            ->orderBy('waktu_masuk', 'asc')
            ->get();
        // dd($data);
        return $data;
    }

    public function headings(): array
    {
        return ["Shift", "Kondisi", "Masuk", "Pulang", "Lokasi Masuk", "Lokasi Pulang", "Titik Masuk", "Titik Pulang", "No Kendaraan", "HM Awal", "HM Akhir", "Catatan Masuk", "Catatan Pulang"];
    }
}
