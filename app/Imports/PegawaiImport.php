<?php

namespace App\Imports;

use App\pegawai;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithStartRow;
use DB;
// use Maatwebsite\Excel\Concerns\SkipsEmptyRows;

class PegawaiImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    use Importable;
    public function model(array $row)
    {
        if(!array_filter($row)) {
            return null;
        };
        
        $id_pegawai = Str::upper(Str::uuid());
        $id_user = Str::upper(Str::uuid());
        $pegawai = [
            'id' => $id_pegawai,
            'id_badge' => $row['0'],
            'nama' => $row['1'],
            'posisi' => $row['2'],
        ];
        // dd($pegawai);
        $result = pegawai::firstOrCreate(
            [ 'id_badge' => str_replace(' ', '', $pegawai['id_badge']) ],
            [
                'id' => $pegawai['id'],
                'id_badge' => str_replace(' ', '', $pegawai['id_badge']),
                'nama' => $pegawai['nama'],
                'posisi' => $pegawai['posisi'],
            ]
        );

        // $result = pegawai::firstOrCreate

        if ($result->id_badge) {
            $username = $result->id_badge;
        } else {
            // $username =  Str::lower(str_replace(' ', '_', $row['nama']));
            $username =  Str::lower(str_replace(' ', '_', $result->nama));
        }
        // dd($id_pegawai);
        $user = User::firstOrCreate(
            [ 'email' => $username , 'id_pegawai' => $id_pegawai],
            [
                'id' => $id_user,
                'name' => $result->nama,
                'id_pegawai' => $id_pegawai,
                'email' => $username,
                'role' => 'Pegawai',
                'password' => bcrypt('12345'),
            ]
        );

        return [$result, $user];
        // return 'Done';
    }

    public function startRow(): int
    {
        return 1;
    }

    // public static function beforeImport(BeforeImport $event)
    // {
    //     $worksheet = $event->reader->getActiveSheet();
    //     $highestRow = $worksheet->getHighestRow(); // e.g. 10

    //     dd($highestRow);
    // }
}
