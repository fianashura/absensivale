<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('cekIDBadge/{id_badge}', 'cekPegawai@index')->name('cekPegawai');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::prefix('pegawai')->group(function() {
    Route::get('/', 'pegawaiController@index')->name('pegawai');
    Route::post('/save', 'pegawaiController@store')->name('pegawai.save');
    Route::post('/import', 'pegawaiController@import')->name('pegawai.import');
    Route::get('/edit/{id}', 'pegawaiController@edit')->name('pegawai.edit');
    Route::post('/update/{id}', 'pegawaiController@update')->name('pegawai.update');
    Route::get('/delete/{id}', 'pegawaiController@destroy')->name('pegawai.delete');
});

Route::prefix('admin')->group(function() {
    Route::get('/', 'adminController@index')->name('admin');
    Route::post('/save', 'adminController@store')->name('admin.save');
    Route::get('/edit/{id}', 'adminController@edit')->name('admin.edit');
    Route::post('/update/{id}', 'adminController@update')->name('admin.update');
    Route::get('/delete/{id}', 'adminController@destroy')->name('admin.delete');
});

Route::prefix('jadwal')->group(function() {
    Route::get('/', 'jadwalController@index')->name('jadwal');
    Route::post('/', 'jadwalController@store')->name('jadwal.save');
    Route::get('/edit/{id}', 'jadwalController@edit')->name('jadwal.edit');
    Route::post('/update/{id}', 'jadwalController@update')->name('jadwal.update');
    Route::get('/delete/{id}', 'jadwalController@destroy')->name('jadwal.delete');
});

Route::prefix('absensi')->group(function() {
    Route::get('/', 'absensiController@index')->name('absensi');
    Route::get('/export/{tanggal}', 'absensiController@export')->name('absensi.export');
    Route::get('/download/{tanggal}', 'absensiController@downloadPictures')->name('absensi.download');
    Route::post('/save', 'absensiController@store')->name('absensi.save');
    Route::post('/saveAbsen', 'absensiController@storeAbsen')->name('absensi.saveAbsen');
    Route::get('/edit/{id}', 'absensiController@edit')->name('absensi.edit');
    Route::post('/update/{id}', 'absensiController@update')->name('absensi.update');
    Route::get('/delete/{id}', 'absensiController@destroy')->name('absensi.delete');
    Route::get('/cekAbsen/{waktu}', 'absensiController@cekAbsen')->name('absensi.cek');
    Route::get('/logout', 'absensiController@absenLogout')->name('absensi.logout');
});

Route::prefix('rekap')->group(function() {
    Route::get('/', 'rekapController@indexRekap')->name('rekap');
    Route::post('/filter', 'rekapController@dateRekap')->name('rekap.date');
    Route::get('/periode', 'rekapController@rekapPeriode')->name('rekap.periode');
    Route::post('/periode/filter', 'rekapController@dateRekapPeriode')->name('rekap.datePeriode');
    Route::get('/periode/export/{from}/{to}', 'rekapController@exportRekapPeriode')->name('rekap.exportPeriode');
    Route::get('/pegawai/{id}', 'rekapController@rekapByIdPegawai')->name('rekap.pegawai');
    Route::post('/pegawai/{id}', 'rekapController@dateRekapByIdPegawai')->name('rekap.datePegawai');
    Route::get('/pegawai/export/{month}/{id}', 'rekapController@exportPegawai')->name('rekap.exportPegawai');
    Route::post('/save', 'rekapController@store')->name('rekap.save');
    Route::get('/edit/{id}', 'rekapController@edit')->name('rekap.edit');
    Route::post('/update/{id}', 'rekapController@update')->name('rekap.update');
    Route::get('/delete/{id}', 'rekapController@destroy')->name('rekap.delete');
    Route::get('/changeToImage/{from}/{to}', 'rekapController@changeToImage')->name('rekap.change');
});
